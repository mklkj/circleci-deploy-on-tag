# CircleCI - deploy on tag

[![CircleCI](https://circleci.com/gh/mklkj/circleci-deploy-on-tag/tree/master.svg?style=svg)](https://circleci.com/gh/mklkj/circleci-deploy-on-tag/tree/master)

```yaml
workflows:
  version: 2
  build-n-deploy:
    jobs:
      - build:
          filters:
            tags:
              only: /.*/
      - test:
          requires:
            - build
          filters:
            tags:
              only: /.*/
      - deploy:
          requires:
            - test
          filters:
            tags:
              only: /\d+\.\d+\.\d+/
            branches:
              ignore: /.*/
```
